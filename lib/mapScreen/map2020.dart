import 'dart:convert';
import 'package:difteriapp/model/modelMap.dart';
import 'package:syncfusion_flutter_maps/maps.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Mapping2020 extends StatefulWidget {
  @override
  _Mapping2020State createState() => _Mapping2020State();
}

class _Mapping2020State extends State<Mapping2020> {
  MapZoomPanBehavior _zoomPanBehavior;
  List<dynamic> listData;
  List<modelMap> mapData = [];

  getModels() async {
    var apiURL = "http://10.0.2.2:8000/api/getData2020";
    var apiResult = await http.get(Uri.parse(apiURL));
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> listData = jsonObject;
    setState(() {
      for (var i = 0; i < listData.length; i++) {
        mapData.add(modelMap.createData(listData[i]));
      }
    });
  }

  @override
  void initState() {
    getModels();
    _zoomPanBehavior =
        MapZoomPanBehavior(zoomLevel: 2);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          color: Colors.teal,
          child: Card(
            child: mapData.isEmpty ? Container(
                      child: Center(child: Text("Loading....")),
                    ) : 
                    ListView.builder(
                      itemCount: 1,
                      itemBuilder: (context, i) {
                      return Padding(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: SizedBox(
                          height: 500,
                          child: SfMaps(
                            layers: [
                              MapShapeLayer(
                                source: MapShapeSource.asset(
                                    'assets/Surabaya.geojson',
                                    shapeDataField: 'name',
                                    dataCount: mapData.length,
                                    primaryValueMapper: (int i) =>
                                        mapData[i].state,
                                    dataLabelMapper: (int i) =>
                                        mapData[i].stateCode,
                                        shapeColorValueMapper: (int i) =>
                                        mapData[i].cluster,
                                        shapeColorMappers: [
                                          MapColorMapper(value: "C1", color: Colors.green, text: 'Kerawanan Rendah'),
                                          MapColorMapper(value: "C2", color: Colors.amber, text: 'Kerawanan Sedang'),
                                          MapColorMapper(value: "C3", color: Colors.red, text: 'Kerawanan Tinggi'),
                                        ],
                                        ),
                                showDataLabels: true,
                                legend: MapLegend(MapElement.shape, offset: Offset(0, 495)),
                                zoomPanBehavior: _zoomPanBehavior,
                              )
                            ],
                          ),
                        ),
                      );
                      }
                    ),
        )));
  }
}

