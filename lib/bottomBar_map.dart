
import 'package:difteriapp/mapScreen/map2016.dart';
import 'package:difteriapp/mapScreen/map2017.dart';
import 'package:difteriapp/mapScreen/map2018.dart';
import 'package:difteriapp/mapScreen/map2019.dart';
import 'package:difteriapp/mapScreen/map2020.dart';
import 'package:flutter/material.dart';

class BottomBarMap extends StatefulWidget {
  @override
  _BottomBarMapState createState() => _BottomBarMapState();
}

class _BottomBarMapState extends State<BottomBarMap> {
  int _selectedIndex = 0;
  final _layoutPage =[
    Mapping2016(),
    Mapping2017(),
    Mapping2018(),
    Mapping2019(),
    Mapping2020(),

  ];
  void _onTabItem(int index){
    setState((){
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            'Peta Kerawanan Difteri Surabaya',
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(  
        backgroundColor: Colors.teal,
        fixedColor: Colors.white,
        iconSize: 35,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.map_rounded),
            label: "2016",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.map_rounded),
            label: "2017",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.map_rounded),
            label: "2018",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.map_rounded),
            label: "2019",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.map_rounded),
            label: "2020",
          )
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onTabItem,
      ),
    );
  }
}