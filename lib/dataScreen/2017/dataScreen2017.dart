import 'package:difteriapp/dataScreen/2017/rendah2017.dart';
import 'package:difteriapp/dataScreen/2017/sedang2017.dart';
import 'package:difteriapp/dataScreen/2017/tinggi2017.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class DataScreen2017 extends StatefulWidget {
  @override
  _DataScreen2017State createState() => _DataScreen2017State();
}

class _DataScreen2017State extends State<DataScreen2017> {
  List _data = [];
  void getModels() async {
    var apiURL = "http://10.0.2.2:8000/api/getData2017";
    var apiResult = await http.get(Uri.parse(apiURL));

    setState(() {
      _data = json.decode(apiResult.body);
    });
  }

@override
  void initState() {
    getModels();
    super.initState();
  }

 SingleChildScrollView _tabelData(){
   return SingleChildScrollView(
     scrollDirection: Axis.vertical,
     child: SingleChildScrollView(
       scrollDirection: Axis.horizontal,
       child: DataTable(
         columnSpacing: 15,
         columns: [
           DataColumn(label: Text("Kecamatan")),
           DataColumn(label: Text("Jumlah Kasus")),
           DataColumn(label: Text("Jumlah Vaksin DPT")),
         ],
         rows: _data.map((data) => DataRow(
           cells: <DataCell>[
          DataCell(Text(data["kecamatan"])),
          DataCell(Center(child: Text(data["jml_kasus"].toString()))),
          DataCell(Center(child: Text(data["jml_vaksin_dpt"].toString()))),
         ])).toList()
         ),
       ),
     );
 }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(8),
        child: ListView(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: (){
                  Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Rendah2017()));
                },
                  child: Text('Rendah'),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green
                  ),
                  ),
                  SizedBox(height: 15),
                  ElevatedButton(
                  onPressed: (){
                  Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Sedang2017()));
                },
                  child: Text('Sedang'),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.amber
                  ),
                  ),
                  SizedBox(height: 15),
                  ElevatedButton(
                  onPressed: (){
                  Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Tinggi2017()));
                },
                  child: Text('Tinggi'),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.red
                  ),
                  )
              ],
            ),
            SizedBox(height: 5),
            _tabelData()
          ],
        ),
      ),
    );
  }
}
