import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Tinggi2018 extends StatefulWidget {
  @override
  _Tinggi2018State createState() => _Tinggi2018State();
}

class _Tinggi2018State extends State<Tinggi2018> {
  List _data = [];
  void getModels() async {
    var apiURL = "http://10.0.2.2:8000/api/tinggi2018";
    var apiResult = await http.get(Uri.parse(apiURL));

    setState(() {
      _data = json.decode(apiResult.body);
    });
  }

  @override
  void initState() {
    getModels();
    super.initState();
  }

  SingleChildScrollView _tabelData() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
            columnSpacing: 15,
            columns: [
              DataColumn(label: Text("Kecamatan")),
              DataColumn(label: Text("Jumlah Kasus")),
              DataColumn(label: Text("Jumlah Vaksin DPT")),
            ],
            rows: _data
                .map((data) => DataRow(cells: <DataCell>[
                      DataCell(Text(data["kecamatan"])),
                      DataCell(
                          Center(child: Text(data["jml_kasus"].toString()))),
                      DataCell(Center(
                          child: Text(data["jml_vaksin_dpt"].toString()))),
                    ]))
                .toList()),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              'assets/img/research.png',
              height: 25,
              width: 25,
              ),
              SizedBox(width: 5),
            Text(
              "Data Kasus & Vaksinasi Difteri",
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(8),
        child: ListView(
          children: <Widget>[
            SizedBox(height: 15),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Tingkat Kerawanan Tinggi',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 15),
                  Icon(
                    Icons.circle,
                    color: Colors.red,
                    )
                ],
              ),
            ),
            SizedBox(height: 15),
            _tabelData()
          ],
        ),
      ),
    );
  }
}
