import 'package:difteriapp/artikel/artikel_1.dart';
import 'package:difteriapp/artikel/artikel_2.dart';
import 'package:difteriapp/artikel/artikel_3.dart';
import 'package:flutter/material.dart';

class InfoNavScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFdde0e3),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 40),
            Image.asset(
              'assets/img/pic/website.png',
              height: 25,
              width: 25,
              ),
              SizedBox(width: 5),
            Text(
              "Info Penyakit Difteri",
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              SizedBox(height: 10),
              Expanded(
                child: GridView.count(
                  childAspectRatio: (100/40),
                  crossAxisCount: 1,
                  mainAxisSpacing: 0,
                  crossAxisSpacing: 0,
                  primary: false,
                  children: [
                    Card(
                      margin: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                      ),
                      elevation: 5,
                      //color: Colors.grey,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Artikel1()),
                          );
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [ 
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: Image(
                                  height: 160,
                                  width: 160,
                                  image: AssetImage(
                                    'assets/img/dif3.jpg',
                                    ),
                                ),
                            ),
                              Text(
                                "Apa Itu Difteri?",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                                ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      margin: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      elevation: 5,
                      //color: Colors.grey,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Artikel2()),
                          );
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [ 
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: Image(
                                  height: 160,
                                  width: 160,
                                  image: AssetImage(
                                    'assets/img/difteri.jpg',
                                    ),
                                ),
                            ),
                              Text(
                                "Gejala Penyakit \nDifteri",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                                ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      margin: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                      ),
                      elevation: 5,
                      //color: Colors.grey,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Artikel3()),
                          );
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [ 
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: Image(
                                  height: 160,
                                  width: 160,
                                  image: AssetImage(
                                    'assets/img/difteri2.jpg',
                                    ),
                                ),
                            ),
                              Text(
                                "Pencegahan Difteri",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                                ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
