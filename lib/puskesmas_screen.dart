import 'package:flutter/material.dart';
import 'package:difteriapp/model/model_pus.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';

class puskesmasScreen extends StatefulWidget {
  @override
  _puskesmasScreenState createState() => _puskesmasScreenState();
}

class _puskesmasScreenState extends State<puskesmasScreen> {
  get id => null;

  getModels() async {
    var apiURL = "http://10.0.2.2:8000/api/puskesmas";
    var apiResult = await http.get(Uri.parse(apiURL));
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> listData = jsonObject;

    List<model_pus> models_pus = [];
    for (var i = 0; i < listData.length; i++) {
      models_pus.add(model_pus.createData(listData[i]));
    }
    return models_pus;
  }

  _callNumber(String phoneNumber) async {
    String number = phoneNumber;
    await FlutterPhoneDirectCaller.callNumber(number);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 40),
            Image.asset(
              'assets/img/pic/hospital.png',
              height: 25,
              width: 25,
              ),
              SizedBox(width: 5),
            Text(
              "Data Puskesmas",
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      body: Container(
        child: Card(
          child: FutureBuilder(
              future: getModels(),
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return Container(
                    child: Center(child: Text("Loading....")),
                  );
                } else {
                  return ListView.separated(
                      separatorBuilder: (context, index) => Divider(
                            color: Colors.black,
                          ),
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, i) {
                        return ListTile(
                          title: Text(
                              'Puskesmas ${snapshot.data[i].nama_puskesmas}'),
                          subtitle: Text('${snapshot.data[i].alamat}'),
                          trailing: IconButton(
                                onPressed: () {
                                  _callNumber(snapshot.data[i].telp_puskesmas);
                                },
                                icon: const Icon(
                                  Icons.phone,
                                  color: Colors.teal,
                                  size: 25,
                                  ),
                              ),
                          // trailing: Text('${snapshot.data[i].telp_puskesmas}'),
                        );
                      });
                }
              }),
        ),
      ),
    );
  }
}
