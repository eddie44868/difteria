import 'package:flutter/material.dart';

class Artikel2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 40),
            Image.asset(
              'assets/img/pic/website.png',
              height: 25,
              width: 25,
            ),
            SizedBox(width: 5),
            Text(
              "Info Penyakit Difteri",
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Center(
              child: Image(
                image: AssetImage('assets/img/ark.jpg'),
                height: 260,
                width: 400,
              ),
            ),
            SizedBox(height: 5),
            Text(
              "Gejala Difteri",
              style: TextStyle(fontSize: 27, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 10),
            Text(
              "     Umumnya, butuh waktu sekitar 2 sampai 5 hari untuk gejala difteri muncul setelah masuk ke dalam tubuh. "
              "Namun sayangnya, kadang penyakit ini tidak menunjukkan gejala apapun." 
              "\n\nSecara umum, penyakit difteri dapat dikenali dari gejala-gejalanya yang dapat timbul. Berikut beberapa gejala yang dapat timbul :"
              "\n 1. Sakit kepala."
              "\n 2. Demam dan menggigil"
              "\n 3. Sakit pada tenggorokan."
              "\n 4. Sulit bernapas."
              "\n 5. Tubuh terasa lemas."
              "\n 6. Leher membengkak (bullneck)."
             ,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.normal, height: 1.3),
              textAlign: TextAlign.justify,
            ),
          ],
        ),
      ),
    );
  }
}
