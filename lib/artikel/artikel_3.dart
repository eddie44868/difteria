import 'package:flutter/material.dart';

class Artikel3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 40),
            Image.asset(
              'assets/img/pic/website.png',
              height: 25,
              width: 25,
            ),
            SizedBox(width: 5),
            Text(
              "Info Penyakit Difteri",
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Center(
              child: Image(
                image: AssetImage('assets/img/dif2.jpg'),
                height: 260,
                width: 400,
              ),
            ),
            SizedBox(height: 5),
            Text(
              "Pencegahan Difteri",
              style: TextStyle(fontSize: 27, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 10),
            Text(
              "     Setelah mengetahui gejala-gejala difteri yang dapat terjadi, kamu juga harus mengetahui cara mencegahnya. Dengan begitu, tubuh kamu benar-benar kuat saat terserang bakteri berbahaya tersebut. "
              "\n\nHanya dengan menjaga kebersihan, serta mengonsumsi makanan yang sehat saja tidak cukup untuk mencegah penyakit difteri. "
              "Pencegahan difteri yang paling efektif adalah dengan melakukan imunisasi. Ikatan Dokter Anak Indonesia (IDAI) menyarankan untuk imunisasi difteri lengkap sebagai pencegahan sesuai dengan usia. "
              "\n\nBerikut adalah pembagian waktu vaksin yang dapat dilakukan : "
              "\n 1. Usia kurang dari 1 tahun wajib mendapatkan 3 kali imunisasi difteri (DPT)."
              "\n\n 2. Anak usia 1 sampai 5 tahun wajib mendapatkan imunisasi ulangan untuk difteri sebanyak 2 kali."
              "\n\n 3. Anak usia sekolah wajib mendapatkan imunisasi difteri melalui program BIAS untuk siswa sekolah dasar (SD) kelas 1, kelas 2, dan kelas 3 atau kelas 5."
              "\n\n 4. Setelah itu, imunisasi harus dilakukan setiap 10 tahun, termasuk untuk orang dewasa. Apabila kamu belum melakukan imunisasi lengkap, segera lakukan hal tersebut di fasilitas kesehatan terdekat."
             ,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.normal, height: 1.3),
              textAlign: TextAlign.justify,
            ),
          ],
        ),
      ),
    );
  }
}
