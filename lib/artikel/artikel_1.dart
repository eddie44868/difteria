import 'package:flutter/material.dart';

class Artikel1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 40),
            Image.asset(
              'assets/img/pic/website.png',
              height: 25,
              width: 25,
            ),
            SizedBox(width: 5),
            Text(
              "Info Penyakit Difteri",
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Center(
              child: Image(
                image: AssetImage('assets/img/dif.jpg'),
                height: 260,
                width: 400,
              ),
            ),
            SizedBox(height: 5),
            Text(
              "Apa Itu Difteri?",
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 10),
            Text(
              "     Difteri adalah salah satu penyakit yang dapat menyebabkan ancaman serius hingga berdampak pada kematian jika terjadi. Selain itu, selalu ada kasus yang terjadi tiap bulannya di provinsi seluruh Indonesia."
              " \n\nSalah satu kasus yang terjadi di Sumatera Utara hingga Oktober 2019 disebutkan 17 orang positif mengalami difteri. Dari total angka tersebut, 3 orang di antaranya mengalami kehilangan nyawa. Bahkan, catatan tersebut selalu naik tiap tahunnya meski vaksin terhadap difteri sudah tersedia."
              "\n\nMaka dari itu, kamu harus benar-benar tahu gejala dari difteri dan cara pencegahan penyakit tersebut. Dengan begitu, gangguan tersebut tidak mudah menyerangmu."
              "\n\n                    Lalu, apa itu difteri?\n\n"
              "     Difteri adalah sebuah penyakit yang disebabkan oleh bakteri bernama Corynebacterium diphtheriae. Gangguan ini menyerang selaput lendir pada hidung dan tenggorokan seseorang. Dalam kasus yang jarang, penyakit ini pun dapat memengaruhi kulit. Gangguan ini termasuk dalam jenis penyakit serius yang sangat menular dan bisa berakibat fatal bagi pengidapnya."
              "\n\nBakteri ini dapat menyebar dengan sangat mudah, apalagi pada seseorang yang belum pernah mendapatkan vaksin difteri. Maka dari itu, sangat penting untuk mengetahui cara penyebarannya agar dapat menjadi pencegahan difteri untuk menyerang. "
              "\n\nPenularannya pun dengan cara-cara umum sederhana, seperti : "
              "\n\n 1. Ketika seseorang menghirup udara yang mengandung percikan air liur pengidap saat bersin atau batuk."
              "\n\n 2. Kontak langsung dengan luka borok pada kulit pengidap. Biasanya penularan ini terjadi oleh pengidap yang tinggal di lingkungan yang kurang bersih."
              "\n\n 3. Melalui barang yang terkontaminasi oleh bakteri, seperti handuk, alat makanan, dan lain-lain."
              "\n\n     Selain penyebarannya yang mudah terjadi, bakteri ini juga dapat menimbulkan gangguan yang fatal. Hal ini disebabkan karena bakteri tersebut menghasilkan racun yang membunuh sel-sel sehat dalam tenggorokan."
              "\n\nPada akhirnya, kumpulan sel mati tersebut dapat membentuk lapisan abu-abu pada tenggorokan. Racun dari bakteri juga dapat menyebar ke aliran darah yang menyebabkan kerusakan pada jantung, ginjal dan sistem saraf. "
              "\n\nDengan mengetahui dampak yang dapat terjadi ketika penyakit tersebut menyerang, maka penting untuk memastikan kamu dan orang di sekitarmu tidak mengalami gejala dari difteri. Selain itu, cara pencegahan difteri juga sangat penting untuk dilakukan.",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.normal, height: 1.5),
              textAlign: TextAlign.justify,
            ),
          ],
        ),
      ),
    );
  }
}
