import 'package:difteriapp/bottomBar_map.dart';
import 'package:difteriapp/bottom_bar.dart';
import 'package:difteriapp/infoNav.dart';
import 'package:difteriapp/puskesmas_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

    void run2016() async {
    var apiURL = "http://10.0.2.2:8000/api/cluster2016";
    var apiResult = await http.get(Uri.parse(apiURL));
  }
  void run2017() async {
    var apiURL = "http://10.0.2.2:8000/api/cluster2017";
    var apiResult = await http.get(Uri.parse(apiURL));
  }
  void run2018() async {
    var apiURL = "http://10.0.2.2:8000/api/cluster2018";
    var apiResult = await http.get(Uri.parse(apiURL));
  }
  void run2019() async {
    var apiURL = "http://10.0.2.2:8000/api/cluster2019";
    var apiResult = await http.get(Uri.parse(apiURL));
  }
  void run2020() async {
    var apiURL = "http://10.0.2.2:8000/api/cluster2020";
    var apiResult = await http.get(Uri.parse(apiURL));
  }

  @override
  void initState() {
    run2016();
    run2017();
    run2018();
    run2019();
    run2020();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xFFdde0e3),
      body: Stack(
        children: [
          Container(
            height: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/img/back.png'),
              fit: BoxFit.fill,
            )),
          ),
          Column(
            children: [
              SizedBox(height: 35),
              Text(
                'Hai Pecinta Kesehatan',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                'Sudahkah kamu bersih hari ini?',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(
                    image: AssetImage('assets/img/test-results.png'),
                    height: 90,
                    width: 120,
                  ),
                  Image(
                    image: AssetImage('assets/img/research.png'),
                    height: 90,
                    width: 90,
                  ),
                ],
              ),
              Expanded(
                child: GridView.count(
                  crossAxisCount: 2,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                  primary: false,
                  children: [
                    Card(
                      margin: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      elevation: 4,
                      //color: Colors.grey,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => BottomBarMap()),
                          );
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image(
                              height: 120,
                              width: 120,
                              image: AssetImage(
                                'assets/img/pic/worldwide.png',
                              ),
                            ),
                            SizedBox(height: 10),
                            Text(
                              "Peta Kerawanan",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      margin: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      elevation: 4,
                      //color: Colors.grey,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => puskesmasScreen()),
                          );
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image(
                              height: 120,
                              width: 120,
                              image: AssetImage(
                                'assets/img/pic/hospital.png',
                              ),
                            ),
                            SizedBox(height: 10),
                            Text(
                              "Puskesmas",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      margin: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      elevation: 4,
                      //color: Colors.grey,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => InfoNavScreen()),
                          );
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image(
                              height: 120,
                              width: 120,
                              image: AssetImage(
                                'assets/img/pic/website.png',
                              ),
                            ),
                            SizedBox(height: 10),
                            Text(
                              "Info Penyakit Difteri",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      margin: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      elevation: 4,
                      //color: Colors.grey,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => BottomBar()),
                          );
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image(
                              height: 120,
                              width: 120,
                              image: AssetImage(
                                'assets/img/pic/statistics.png',
                              ),
                            ),
                            SizedBox(height: 10),
                            Text(
                              "Data Difteria",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
