import 'dart:convert';
import 'package:http/http.dart' as http;

class model {
  String kecamatan;
  String jml_kepadatan;
  String jml_rumahtdksehat;
  String jml_vaksin_dpt;
  String jml_kasus;
  String tahun;
  String cluster;
  // model({this.kecamatan, this.cluster, this.tahun});
  model({this.kecamatan, this.jml_kepadatan, this.jml_rumahtdksehat, this.jml_vaksin_dpt, this.jml_kasus, this.tahun, this.cluster});

  factory model.createData (Map<String, dynamic> object) {
    return model(
      kecamatan: object['kecamatan'].toString(),
      jml_kepadatan: object['jml_kepadatan'].toString(),
      jml_rumahtdksehat: object['jml_rumahtdksehat'].toString(),
      jml_vaksin_dpt: object['jml_vaksin_dpt'].toString(),
      jml_kasus: object['jml_kasus'].toString(),
      tahun: object['tahun'].toString(),
      cluster: object['cluster'].toString(),
    );
  }

  static Future <List<model>> getModels() async {
    var apiURL = "http://10.0.2.2:8000/api/data_difteria" ;
    var apiResult = await http.get(Uri.parse(apiURL));
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> listData = jsonObject;

    List<model> models = [];
    for (var i = 0; i < listData.length; i++) {
      models.add(model.createData(listData[i]));
    }
    return models;
  }
}