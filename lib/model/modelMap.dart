import 'dart:convert';
import 'package:http/http.dart' as http;

class modelMap {
  String state;
  String stateCode;
  String cluster;
  modelMap({this.state, this.stateCode, this.cluster});


  factory modelMap.createData (Map<String, dynamic> object) {
    return modelMap(
      state: object['kecamatan'].toString(),
      stateCode: object['kecamatan'].toString(),
      cluster: object['cluster'].toString(),
    );
  }

  static Future <List<modelMap>> getModels() async {
    var apiURL = "http://10.0.2.2:8000/api/data_difteria" ;
    var apiResult = await http.get(Uri.parse(apiURL));
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> listData = jsonObject;

    List<modelMap> models = [];
    for (var i = 0; i < listData.length; i++) {
      models.add(modelMap.createData(listData[i]));
    }
    return models;
  }
}