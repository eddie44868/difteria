class model_pus {
  String kecamatan;
  String nama_puskesmas;
  String telp_puskesmas;
  String alamat;
  
  // model({this.kecamatan, this.cluster, this.tahun});
  model_pus({this.kecamatan, this.nama_puskesmas, this.telp_puskesmas, this.alamat});

  factory model_pus.createData (Map<String, dynamic> object) {
    return model_pus(
      kecamatan: object['kecamatan'].toString(),
      nama_puskesmas: object['nama_puskesmas'].toString(),
      telp_puskesmas: object['telp_puskesmas'].toString(),
      alamat: object['alamat'].toString(),
    );
  }

//   static Future <List<model_pus>> getModels() async {
//     var apiURL = "http://10.0.2.2:8000/api/puskesmas" ;
//     var apiResult = await http.get(Uri.parse(apiURL));
//     var jsonObject = json.decode(apiResult.body);
//     List<dynamic> listData = jsonObject;

//     List<model_pus> models_pus = [];
//     for (var i = 0; i < listData.length; i++) {
//       models_pus.add(model_pus.createData(listData[i]));
//     }
//     return models_pus;
//   }
}