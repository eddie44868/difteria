import 'package:difteriapp/dataScreen/2016/rendah2016.dart';
import 'package:difteriapp/dataScreen/2016/sedang2016.dart';
import 'package:difteriapp/dataScreen/2016/tinggi2016.dart';
import 'package:difteriapp/dataScreen/2016/dataScreen2016.dart';
import 'package:difteriapp/dataScreen/2017/dataScreen2017.dart';
import 'package:difteriapp/dataScreen/2017/rendah2017.dart';
import 'package:difteriapp/dataScreen/2018/dataScreen2018.dart';
import 'package:difteriapp/dataScreen/2019/dataScreen2019.dart';
import 'package:difteriapp/dataScreen/2020/dataScreen2020.dart';
import 'package:flutter/material.dart';

class BottomBar extends StatefulWidget {
  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int _selectedIndex = 0;
  final _layoutPage =[
   DataScreen2016(),
   DataScreen2017(),
   DataScreen2018(),
   DataScreen2019(),
   DataScreen2020()

  ];
  void _onTabItem(int index){
    setState((){
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              'assets/img/research.png',
              height: 25,
              width: 25,
              ),
              SizedBox(width: 5),
            Text(
              "Data Kasus & Vaksinasi Difteri",
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(  
        backgroundColor: Colors.teal,
        fixedColor: Colors.white,
        iconSize: 35,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: "2016",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: "2017",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: "2018",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: "2019",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: "2020",
          )
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onTabItem,
      ),
    );
  }
}